# YAWeS #
**YAWeS** (**Y**et **A**nother python-based **We**b **S**ervice) is a Restful API web service 
built on top of [ASAB Micro Framework](https://github.com/TeskaLabs/asab).

### Takeaway comments/works 
- The documentation of [ASAB Framework](https://asab.readthedocs.io/en/latest/) is bad (`Not that good`), 
    yet they claimed that theirs are `well-documented`
- I did this work by migrating / re-cycling some useful codes from my own 
    [public repository](https://github.com/ardihikaru/flask-api)
- GIT Branching Model can help in managing and controlling the development. 
    I used [this Model](https://nvie.com/posts/a-successful-git-branching-model/)

### To dos 
- [x] Understanding the test requirements
- [x] Understanding the usage of used Stack
    - [x] Understand the usage of [ASAB Framework](https://asab.readthedocs.io/en/latest/)
    - [x] Understand the usage of MongoDB
- [x] Note down main tasks
    - [x] Provide with estimate how long would it take you to do it: 
        **2 ~ 4 Hours** with available and clear Boilerplate (Except Code Documentation; It takes time)
    - [x] [Add JWT procedure to get data](https://bitbucket.org/ardihikaru3/balabali/src/master/app.py)
    - [x] [Header name : base64 encoded of username and pass joined by double colon "Aladdin:OpenSesame"](https://bitbucket.org/ardihikaru3/balabali/src/master/app.py)
    - [x] [Project MUST have configuration file where mongo db credentials and and basic auth username and password can be edited](https://bitbucket.org/ardihikaru3/balabali/src/master/routes/user.py)
    - [x] [API: Add GET /exchangerate](https://bitbucket.org/ardihikaru3/balabali/src/master/routes/exchange_rate.py)
    - [x] [API: Add GET /conversion](https://bitbucket.org/ardihikaru3/balabali/src/master/routes/conversion.py)
    - [x] [API: Add GET /conversion/{_id_in_db_in_hexadecimal}](https://bitbucket.org/ardihikaru3/balabali/src/master/routes/conversion.py)
    - [x] [API: Add POST /conversion](https://bitbucket.org/ardihikaru3/balabali/src/master/routes/conversion.py)
- [x] Things to complete the [tasks](https://bitbucket.org/ardihikaru3/balabali/src/master/TASK.md)
    - [x] Find a suitable library to create route Decorator (Codes should be cleaned/refactored well)
    - [x] Find a suitable library to create MongoDB Model (It will be easier to handle with this)
    - [x] Find a suitable library to enable JWT mechanism
- [x] Complete Documentation (README and Codes)

### Requirements 
1. Python 3.6++
2. RedisDB 
3. [ASAB Framework](https://github.com/TeskaLabs/asab)
4. MongoDB

### How to use 
1. Install required stacks 
2. Create virtual environment: `$ python3 -m venv venv`
3. Install python libraries: `$ pip install -r requirements.txt`
4. Run RedisDB (Please add password: `bismillah`); 
    [Suggested to use this dockerized version](https://github.com/ardihikaru/flask-api/tree/master/others/redis)
5. Run MongoDB
6. Run main file with the config file (`app.conf`): `$ python app.py -c app.conf`

### Accessible APIs 
* Auth
    - `POST /auth/login`: Login and receive `access_token`
    - `GET /auth/logout`: Logout and delete (revoked/blacklisted) active `access_token`
* Users
    - `POST /users`: Register a new user 
    - `GET /users`: Get all users
    - `GET /users`: Update a specific user (with `JSON Body`); Filter by ID
    - `DELETE /users`: Delete a specific user; Filter by ID (String) or IDs (List)
* Exchange Rate
    - `GET /exchangerate`: Get all available exchange rates
    - `GET /exchangerate?base=<string>`: Get all available exchange rates with a specific base currency
* Conversion
    - `POST /conversion`: Register a new conversion value
    - `GET /conversion`: Get all conversion data
    - `GET /conversion/{_id}`: Get conversion data by ID
    - `PUT /conversion/{_id}`: Update a conversion specific data (with `JSON Body`); Filter by ID
    - `DELETE /users`: Delete a conversion data; Filter by ID (String) or IDs (List)
    
### Contributors 
1. Muhammad Febrian Ardiansyah 
([github](https://github.com/ardihikaru), 
[gitlab](https://gitlab.com/ardihikaru), 
[bitbucket](https://bitbucket.org/ardihikaru3/))

### Important resources 
1. API data sources
    - Main page: https://exchangeratesapi.io/
    - Latest currency link: https://api.exchangeratesapi.io/latest
    - Get specific currency: https://api.exchangeratesapi.io/latest?base=USD
2. GIT Branching Model: https://nvie.com/posts/a-successful-git-branching-model/
3. ASAB Micro Framework: https://github.com/TeskaLabs/asab
4. AIOHTTP Mircro Framework: https://docs.aiohttp.org/en/stable/
5. AIOHTTP JWT library: https://pypi.org/project/aiohttp-jwt/
6. Route Decorator: https://pypi.org/project/aiohttp_route_decorator/0.0.1/

### License 
[MIT](https://choosealicense.com/licenses/mit/)