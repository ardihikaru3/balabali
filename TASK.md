# Sample REST api to MongoDB database

## Task: Make REST api in asab framework.

## Expectations

Just try to do it. It you can't or it can't be done just write why you couldn't do it. It you think you could improve something, write it up.
After you finish put it on some bitbucket repo (as public) and send me the link of it. You can use this as README file.
Provide with estimate how long would it take you to do it. (Don't be afraid of this, just provide some number) 
### Framwework and documentation

* Framework - https://github.com/TeskaLabs/asab
* Documentation for framework - https://asab.readthedocs.io/en/latest/asab/application.html
* Examples - https://github.com/TeskaLabs/asab/tree/master/examples

### Requirements of the task

Make functional REST api for basic CRUD functionality (in this instance it's only Create nad Read).
Databse for backend is MongoDB

#### General requirements
* All project dependencies must be installable from requirements.txt
* Project MUST have configuration file where mongo db credentials and and basic auth username and password can be edited 
* Middlweare (inside asab framework) that extracts basic http auth credentials from headers:

Sample of header authorisation : 
``` 
Authorization: Basic QWxhZGRpbjpPcGVuU2VzYW1l
^^^^^^^^^^^^^        ^^^^^^^^^^^^^^^^^^^^^^^^
 Header name          base64 encoded of username and pass joined by double colon "Aladdin:OpenSesame"
```
#### Endpoints and explanation

##### GET /excangerate
Calling this endpoint should consume api from GET https://api.exchangeratesapi.io/latest and return results that
lists conversion rates for euro. 
response for success:
 ```
 {
     success: true,
     message: null,
     data: {
        "rates": {
            "CAD": 1.5318,
            "HKD": 8.6904,
            "ISK": 155.0,
            "PHP": 56.053,
            "DKK": 7.4523,
            "HUF": 355.15,
            "CZK": 26.808,
            "AUD": 1.6313,
            "RON": 4.8426,
            "SEK": 10.4773,
            "IDR": 15944.89,
            "INR": 84.8215,
            "BRL": 6.0595,
            "RUB": 77.88,
            "HRK": 7.5575,
            "JPY": 119.93,
            "THB": 34.674,
            "CHF": 1.0631,
            "SGD": 1.5608,
            "PLN": 4.4684,
            "BGN": 1.9558,
            "TRY": 7.6865,
            "CNY": 7.9298,
            "NOK": 10.885,
            "NZD": 1.7428,
            "ZAR": 19.3799,
            "USD": 1.1213,
            "MXN": 25.5836,
            "ILS": 3.847,
            "GBP": 0.90575,
            "KRW": 1346.19,
            "MYR": 4.8115
        },
        "base": "EUR",
        "date": "2020-06-26"
    }
 } 
 ```
 response for error:
 ```
 {
     "success": false,
     "message": "server.error",
     "data": null
 }
 ```

#### GET /conversion
returns all conversion in db.
response for success:
```
{ 
    "success": true,
    "message": null,
    "data": [
        {
            _id: "5ef9bb848b40a245f5905b0a",
            "currency": "HRK",
            "conversion_rate": 7.5575,
            "in_eur": 1,
            "to_currency": 7.5575
            "timestamp": "2020-04-01T08:47:04.137+00:00"
        },
        {
            _id: "5ef9bbeb8b40a245f5905b0b",
            "currency": "HRK",
            "conversion_rate": "7.5575",
            "in_eur": 2,
            "to_currency": 15.115
            "timestamp": "2020-04-01T08:48:02.100+00:00"
        },
    ]
}
...
```
response for error:
```
{
    "success": false,
    "message": "server.error",
    "data": null
}
```
#### GET /conversion/<_id_in_db_in_hexadecimal>
return specific conversion
```
GET /conversion/5ef9bb848b40a245f5905b0a
```
and it should return
```
{ 
    "success": true,
    "message": null,
    "data": {
        _id: "5ef9bb848b40a245f5905b0a",
        "currency": "HRK",
        "conversion_rate": 7.5575,
        "in_eur": 1,
        "to_currency": 7.5575
        "timestamp": "2020-04-01T08:47:04.137+00:00"
    }
}
```
response for error:
```
{
    "success": false,
    "message": "server.error",
    "data": null
}
```

#### POST /conversion
creates conversion and returns saved object
post data:
```
{
    "currency": "HRK",
    "conversion_rate": 7.5575,
    "in_eur": 3,
},
```
returns:
```
{ 
    "success": true,
    "message": null,
    "data": {
        _id: "5ef9bdea8b40a245f5905b0c",
        "currency": "HRK",
        "conversion_rate": 7.5575,
        "in_eur": 3,
        "to_currency": 22.6725
        "timestamp": "2020-04-01T09:11:23.125+00:00"
    }
}
```
response for error:
```
{
    "success": false,
    "message": "server.error",
    "data": null
}
```
#### All other endpoints should return 404

