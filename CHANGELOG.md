## 1.0.5 (July 10, 2020)
  - re-enabled JWT Mechanism
  - Merge branch 'hotfix-1.0.4'

## 1.0.4 (July 10, 2020)
  - bug fixed
  - Merge branch 'hotfix-1.0.3'

## 1.0.3 (July 09, 2020)
  - enable JWT mechanism; bug fixed in requirements.txt
  - Merge branch 'hotfix-1.0.2'

## 1.0.2 (七月 09, 2020)
  - update readme file
  - Merge branch 'hotfix-1.0.1'

## 1.0.1 (七月 09, 2020)
  - update readme file
  - Merge branch 'release-1.0'

## 1.0 (七月 09, 2020)
  - Merge branch 'feature/api-testing' into develop
  - change `id` into `_id` to match with test requirement
  - Merge branch 'feature/readme' into develop
  - updated readme;
  - Merge branch 'feature/readme' into develop
  - updated readme; added requirements.txt
  - Merge branch 'feature/code-comments' into develop
  - Added comments and description on each specific code/class/function/file
  - Merge branch 'feature/jwt-sync' into develop
  - changed JWT policy
  - Merge branch 'feature/api/conversion' into develop
  - Multiple updates: - Code refactoring for /api/exchangerate - Added & tested endpoints /api/conversion, included: POST and GET - Added & tested endpoints /api/conversion/{_id}, included: GET, DELETE, and PUT
  - Merge branch 'feature/api/exchange-rate' into develop
  - Multiple updates: - Added & tested endpoint /api/exchangerate - Added & tested /api/exchangerate?base=<string>; e.g. USD, EUR
  - Merge branch 'feature/api/users' into develop
  - Multiple updates: - Added connection with MongoDB - Added connection with RedisDB - Added & implemented database modeling with `mongoengine` library - Added & implemented route decorator with `aiohttp_route_decorator` library - Added & implemented JWT mechanism with `aiohttp_jwt` library - Implemented a custom `get_token` function (currently it simply forward the collected access_token) - Implemented a custom `is_revoked` function to verify collected access_token with the blacklist database - Added & tested endpoints /users with GET, POST, PUT, and DELETE - Added & tested endpoint POST /auth/login - Added & tested endpoint GET /auth/logout
  - Merge branch 'feature/testing' into develop
  - added testing files; verify the usage
  - Merge branch 'feature/readme' into develop
  - Initial information in the README

## 0.1.0 (July 09, 2020)
  - Initial files
  - Initial commit

