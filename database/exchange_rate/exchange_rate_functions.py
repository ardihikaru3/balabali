"""
    List of functions to manage actions (Create, Read, Update, Delete) of EXCHANGE RATE data
"""

import urllib.request
import simplejson as json


def get_all_data(config, base=None):
    try:
        url_data = config["uri"]["exchangerate"]
        if base is not None:
            url_data = config["uri"]["exchangerate"] + "?base=" + base
        web_url = urllib.request.urlopen(url_data)
        data = web_url.read()
        encoding = web_url.info().get_content_charset('utf-8')
        data_dict = json.loads(data.decode(encoding))
    except Exception as e:
        return False, None, 0

    if len(data_dict) > 0:
        return True, data_dict, len(data_dict["rates"])
    else:
        return False, None, 0

