"""
    Class Model for CONVERSIONS Collections
"""

from mongoengine import Document, StringField, DateTimeField, IntField, FloatField
import datetime


class ConversionModel(Document):
    meta = {'collection': 'Conversions'}
    currency = StringField(required=True)
    conversion_rate = StringField(required=True)
    in_eur = IntField(required=True)
    to_currency = FloatField(required=True)
    created_at = DateTimeField(default=datetime.datetime.now)
    updated_at = DateTimeField(default=datetime.datetime.now)

    def update(self, **kwargs):
        kwargs["updated_at"] = datetime.datetime.now()
        return super(ConversionModel, self).update(**kwargs)
