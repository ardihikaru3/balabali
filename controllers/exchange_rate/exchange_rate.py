"""
   This is a Controller class to manage any action related with /api/exchangerate endpoints
"""

from addons.utils import get_json_template
from database.exchange_rate.exchange_rate_functions import get_all_data
import asab
from addons.redis.my_redis import MyRedis


class ExchangeRate(MyRedis):
    def __init__(self):
        super().__init__(asab.Config)
        self.status_code = 200
        self.resp_status = None
        self.resp_data = None
        self.total_records = 0
        self.msg = None
        self.password_hash = None

    def set_status_code(self, code):
        self.status_code = code

    def set_resp_status(self, status):
        self.resp_status = status

    def set_resp_data(self, json_data):
        self.resp_data = json_data

    def set_msg(self, msg):
        self.msg = msg

    def trx_get_data(self, base=None):
        is_valid, users, self.total_records = get_all_data(asab.Config, base)
        self.set_resp_status(is_valid)
        self.set_msg("server.error")
        self.set_resp_data(None)

        if is_valid:
            self.set_msg("Collecting data success.")
            self.set_resp_data(users)

    def get_data(self, base=None):
        self.trx_get_data(base=base)
        return get_json_template(response=self.resp_status, results=self.resp_data, message=self.msg,
                                 total=self.total_records)
