"""
    List of routes for /api/conversion* endpoints
"""

from aiohttp_route_decorator import RouteCollector
import aiohttp
from controllers.conversion.conversion import Conversion as DataController
from addons.utils import get_unprocessable_request

route = RouteCollector()


@route('', methods=['POST', 'GET'])
async def index(request):
    """
        Endpoint to:
         1. GET all conversion data
            Try: curl http://localhost:8080/api/conversion
         2. POST a new conversion data
            Try: curl http://localhost:8080/api/conversion -X POST -H "Content-Type: application/json"
                    -d '{ "currency": "HRK", "conversion_rate": "7.5575", "in_eur": 2, "to_currency": 15.115}'
    """

    if request.method == 'POST':
        try:
            json_data = await request.json()
            resp = DataController().register(json_data)
        except:
            return get_unprocessable_request()

        return aiohttp.web.json_response(resp)

    if request.method == 'GET':
        resp = DataController().get_data()
        return aiohttp.web.json_response(resp)


@route('/{_id}', methods=['GET', 'DELETE', 'PUT'])
async def index_by(request):
    """
        Endpoint to:
         1. GET conversion data by id
            Try: curl http://localhost:8080/api/conversion/{_id}
         2. DELETE conversion data by id
            Try: curl http://localhost:8080/api/conversion/{_id} -X DELETE
         3. PUT (Edit) conversion data by id
            Try: curl http://localhost:8080/api/conversion/{_id}
                    -X POST -H "Content-Type: application/json" -d '{"currency":"EUR"}'
    """


    try:
        _id = str(request.match_info['_id'])
    except:
        return get_unprocessable_request()

    if request.method == 'GET':
        resp = DataController().get_data_by_id(_id)
        return aiohttp.web.json_response(resp)
    elif request.method == 'DELETE':
        resp = DataController().delete_data_by_id(_id)
        return aiohttp.web.json_response(resp)
    elif request.method == 'PUT':
        try:
            json_data = await request.json()
            resp = DataController().update_data_by_id(_id, json_data)
            return aiohttp.web.json_response(resp)
        except:
            return get_unprocessable_request()
    else:
        return get_unprocessable_request()
