"""
    List of routes for /api/exchangerate* endpoints
"""

from aiohttp_route_decorator import RouteCollector
import aiohttp
from controllers.exchange_rate.exchange_rate import ExchangeRate as DataController

route = RouteCollector()


@route('', methods=['GET'])
async def index(request):
    """
        Endpoint to:
         1. GET conversion data by default currency
            Try: curl http://localhost:8080/api/exchangerate
         2. GET conversion data by a specific currency
            Try: curl -X GET 'http://localhost:8080/api/exchangerate?base=USD'
    """

    if request.method == 'GET':
        try:
            base = request.rel_url.query['base']
        except:
            base = None
        resp = DataController().get_data(base)
        return aiohttp.web.json_response(resp)
